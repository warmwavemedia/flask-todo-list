For statr:

```
pipenv install
pipenv run python app.py
```

or

```
pip install -r requirements.txt
python app.py
```


Based on:

```
https://github.com/monicacalle/Todo-List-API-in-Python-Flask/blob/master/src/app.py
```