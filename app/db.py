from datetime import datetime

from loguru import logger
from sqlalchemy import create_engine, Column, Integer, String, Date
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from app.config import today

Base = declarative_base()


class Task(Base):
    __tablename__ = "task"
    id = Column(Integer, primary_key=True)
    task = Column(String(200), nullable=False)
    deadline = Column(Date, default=today)


class DbEngine:
    def __init__(self, db_name):
        engine = create_engine(f"sqlite:///{db_name}?check_same_thread=False")
        Base.metadata.create_all(engine)
        self.session = sessionmaker(bind=engine)()

    def create_task(self, task_text, deadline):
        new_task = Task(task=task_text, deadline=deadline)
        self.session.add(new_task)
        self.session.commit()
        logger.info(f"The task has been added")

    def delete_task(self, task_id):
        task = self.get_task_by_id(task_id)
        self.session.delete(task)
        self.session.commit()
        logger.info(f"The task: {task_id} has been deleted!")

    def get_task_by_id(self, task_id):
        task = self.session.query(Task).filter(Task.id == task_id).all()[0]
        return task

    def get_tasks_by_date(self, day, is_late=False):
        if is_late:
            tasks = self.session.query(Task).filter(Task.deadline < day).all()
        else:
            tasks = self.session.query(Task).filter(Task.deadline >= day).all()
        return tasks

    def get_all_tasks(self):
        tasks = self.session.query(Task).order_by(Task.deadline).all()
        return tasks
