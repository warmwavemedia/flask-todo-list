import sys, os
from datetime import datetime

from loguru import logger

today = datetime.today().date()

fmt = "<cyan>{file: <10}</cyan> : <cyan>{name: <20}</cyan> : <cyan>{function: <15}</cyan> " \
      "[LINE:{line: <4}] " \
      "<level>{level: <7}</level> " \
      "<green>[{time:YYYY-MM-DD HH:mm:ss.SSS}]</green> " \
      "<level>{message}</level>"
logger.remove()
logger.add(
    sys.stderr,
    format=fmt
    # filter="my_module",
    # level="INFO",
)
logger.add(
    f"logs/todo.log",
    format=fmt,
    rotation='00:00',
    retention="10 days",
    compression="zip",
)
