from datetime import datetime

from flask import render_template, request, redirect, url_for

from app import app
from app import db
from app.config import today


@app.route('/', methods=['GET', 'POST'])
def index():
    missed_tasks = db.get_tasks_by_date(today, is_late=True)
    actual_tasks = db.get_tasks_by_date(today, is_late=False)
    tasks = db.get_all_tasks()

    data = {
        'actual_tasks': actual_tasks,
        'missed_tasks': missed_tasks,
        'tasks': tasks,
    }

    if request.method == "POST":
        deadline = datetime.strptime(request.form['deadline'], "%Y-%m-%d")
        task_text = request.form['task_text']

        db.create_task(task_text=task_text, deadline=deadline)

        return redirect(url_for('index'))

    return render_template('index.html', data=data)


@app.route('/complete/<id>')
def complete(id):
    db.delete_task(int(id))

    return redirect(url_for('index'))
