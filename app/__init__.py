from flask import Flask

from app.db import DbEngine

app = Flask(__name__)
db = DbEngine("todo.db")

from app import routes
